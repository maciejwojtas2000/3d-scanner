/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void Trigger()
{
        HAL_GPIO_WritePin (LCD_E_GPIO_Port, LCD_E_Pin, SET );
        HAL_Delay(1);
        HAL_GPIO_WritePin (LCD_E_GPIO_Port, LCD_E_Pin, RESET );
}

void LCD_Instruction (uint8_t DB, uint8_t RS)
{
        HAL_Delay(2);
        HAL_GPIO_WritePin (LCD_E_GPIO_Port, LCD_E_Pin, SET );

        if (RS)
                HAL_GPIO_WritePin (LCD_RS_GPIO_Port, LCD_RS_Pin, SET );
        else
             	HAL_GPIO_WritePin (LCD_RS_GPIO_Port, LCD_RS_Pin, RESET );

        HAL_GPIO_WritePin (LCD_D7_GPIO_Port, LCD_D7_Pin, DB & (1<<7) ? SET : RESET);
        HAL_GPIO_WritePin (LCD_D6_GPIO_Port, LCD_D6_Pin, DB & (1<<6) ? SET : RESET);
        HAL_GPIO_WritePin (LCD_D5_GPIO_Port, LCD_D5_Pin, DB & (1<<5) ? SET : RESET);
        HAL_GPIO_WritePin (LCD_D4_GPIO_Port, LCD_D4_Pin, DB & (1<<4) ? SET : RESET);
        HAL_GPIO_WritePin (LCD_D3_GPIO_Port, LCD_D3_Pin, DB & (1<<3) ? SET : RESET);
        HAL_GPIO_WritePin (LCD_D2_GPIO_Port, LCD_D2_Pin, DB & (1<<2) ? SET : RESET);
        HAL_GPIO_WritePin (LCD_D1_GPIO_Port, LCD_D1_Pin, DB & (1<<1) ? SET : RESET);
        HAL_GPIO_WritePin (LCD_D0_GPIO_Port, LCD_D0_Pin, DB & (1<<0) ? SET : RESET);
        HAL_Delay(1);
        HAL_GPIO_WritePin (LCD_E_GPIO_Port, LCD_E_Pin, RESET );
}



void LCD_Init()
{
        HAL_Delay (65);
        HAL_GPIO_WritePin (LCD_RS_GPIO_Port, LCD_RS_Pin, RESET );
        HAL_GPIO_WritePin (LCD_D7_GPIO_Port, LCD_D7_Pin, RESET);
        HAL_GPIO_WritePin (LCD_D6_GPIO_Port, LCD_D6_Pin, RESET);
        HAL_GPIO_WritePin (LCD_D5_GPIO_Port, LCD_D5_Pin, SET);
        HAL_GPIO_WritePin (LCD_D4_GPIO_Port, LCD_D4_Pin, SET);
        HAL_GPIO_WritePin (LCD_D3_GPIO_Port, LCD_D3_Pin, RESET );
        HAL_GPIO_WritePin (LCD_D2_GPIO_Port, LCD_D2_Pin, RESET);
        HAL_GPIO_WritePin (LCD_D1_GPIO_Port, LCD_D1_Pin, RESET);
        HAL_GPIO_WritePin (LCD_D0_GPIO_Port, LCD_D0_Pin, RESET);

        Trigger();
        HAL_Delay(7);

        Trigger();
        HAL_Delay(5);

        Trigger();
        HAL_Delay(2);
        LCD_Instruction (0b00111000, 0);

        LCD_Instruction (0b00111000, 0);
        LCD_Instruction (0b00001000, 0);
        LCD_Instruction (0b00000001, 0);
        LCD_Instruction (0b00000110, 0);
}


void letter(char c){
	LCD_Instruction (c, 1);
	HAL_Delay(50);
}

void word(char* text){
	while(*text){
		letter(*text++);
		HAL_Delay(50);
}
}




char alphanum()
{
	int flag=1;
	int flaga1=1;
	int flaga2=1;
	int32_t tick2;
	int32_t tick1=HAL_GetTick();
	char symbol;

while(flag){
HAL_GPIO_WritePin (KEY_R1_GPIO_Port,KEY_R1_Pin, RESET);
HAL_GPIO_WritePin (KEY_R2_GPIO_Port,KEY_R2_Pin, SET);
HAL_GPIO_WritePin (KEY_R3_GPIO_Port,KEY_R3_Pin, SET);
HAL_GPIO_WritePin (KEY_R4_GPIO_Port,KEY_R4_Pin, SET);


if(HAL_GPIO_ReadPin(KEY_C2_GPIO_Port, KEY_C2_Pin)==0)
	{

		symbol='a';
		HAL_Delay(50);
		LCD_Instruction (0b00000001, 0);

		LCD_Instruction (symbol, 1);
		HAL_Delay(1000);
		if(HAL_GPIO_ReadPin(KEY_C2_GPIO_Port, KEY_C2_Pin)==0)
		{
			symbol='b';
			HAL_Delay(50);
			LCD_Instruction (0b00000001, 0);
			LCD_Instruction (symbol, 1);
			HAL_Delay(1000);

			if(HAL_GPIO_ReadPin(KEY_C2_GPIO_Port, KEY_C2_Pin)==0){
				symbol='c';
				HAL_Delay(50);
				LCD_Instruction (0b00000001, 0);
				LCD_Instruction (symbol, 1);
				HAL_Delay(1000);

			}

		}
		flag=0;
	}

if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0)
{


	symbol='d';HAL_Delay(50);
	LCD_Instruction (0b00000001, 0);

	LCD_Instruction (symbol, 1);
	HAL_Delay(1000);
	if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0)
	{
		symbol='e';
		HAL_Delay(50);
		LCD_Instruction (0b00000001, 0);
		LCD_Instruction (symbol, 1);
		HAL_Delay(1000);

		if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0){
			symbol='f';
			HAL_Delay(50);
			LCD_Instruction (0b00000001, 0);
			LCD_Instruction (symbol, 1);
			HAL_Delay(1000);

		}

	}
	flag=0;
}



HAL_GPIO_WritePin (KEY_R1_GPIO_Port,KEY_R1_Pin, SET);
HAL_GPIO_WritePin (KEY_R2_GPIO_Port,KEY_R2_Pin, RESET);
HAL_GPIO_WritePin (KEY_R3_GPIO_Port,KEY_R3_Pin, SET);
HAL_GPIO_WritePin (KEY_R4_GPIO_Port,KEY_R4_Pin, SET);

if(HAL_GPIO_ReadPin(KEY_C1_GPIO_Port, KEY_C1_Pin)==0)
	{
		symbol='g';		LCD_Instruction (0b00000001, 0);
		LCD_Instruction (symbol, 1);
		HAL_Delay(1000);
		if(HAL_GPIO_ReadPin(KEY_C1_GPIO_Port, KEY_C1_Pin)==0){
			symbol='h';		LCD_Instruction (0b00000001, 0);
			LCD_Instruction (symbol, 1);
			HAL_Delay(1000);
			if(HAL_GPIO_ReadPin(KEY_C1_GPIO_Port, KEY_C1_Pin)==0){
				symbol='i';		LCD_Instruction (0b00000001, 0);
				LCD_Instruction (symbol, 1);
				HAL_Delay(1000);
			}

		}
		flag=0;
	}




if(HAL_GPIO_ReadPin(KEY_C2_GPIO_Port, KEY_C2_Pin)==0)
	{
		symbol='j';		LCD_Instruction (0b00000001, 0);
		LCD_Instruction (symbol, 1);
		HAL_Delay(1000);
		if(HAL_GPIO_ReadPin(KEY_C2_GPIO_Port, KEY_C2_Pin)==0){
			symbol='k';		LCD_Instruction (0b00000001, 0);
			LCD_Instruction (symbol, 1);
			HAL_Delay(1000);
			if(HAL_GPIO_ReadPin(KEY_C2_GPIO_Port, KEY_C2_Pin)==0){
				symbol='l';		LCD_Instruction (0b00000001, 0);
				LCD_Instruction (symbol, 1);
				HAL_Delay(1000);
			}

		}
		flag=0;
	}


if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0)
	{
		symbol='m';		LCD_Instruction (0b00000001, 0);
		LCD_Instruction (symbol, 1);
		HAL_Delay(1000);
		if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0){
			symbol='n';		LCD_Instruction (0b00000001, 0);
			LCD_Instruction (symbol, 1);
			HAL_Delay(1000);
			if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0){
				symbol='o';		LCD_Instruction (0b00000001, 0);
				LCD_Instruction (symbol, 1);
				HAL_Delay(1000);
			}

		}
		flag=0;
	}



HAL_GPIO_WritePin (KEY_R1_GPIO_Port,KEY_R1_Pin, SET);
HAL_GPIO_WritePin (KEY_R2_GPIO_Port,KEY_R2_Pin, SET);
HAL_GPIO_WritePin (KEY_R3_GPIO_Port,KEY_R3_Pin, RESET);
HAL_GPIO_WritePin (KEY_R4_GPIO_Port,KEY_R4_Pin, SET);

if(HAL_GPIO_ReadPin(KEY_C1_GPIO_Port, KEY_C1_Pin)==0)
	{
		symbol='p';		LCD_Instruction (0b00000001, 0);
		LCD_Instruction (symbol, 1);
		HAL_Delay(1000);
		if(HAL_GPIO_ReadPin(KEY_C1_GPIO_Port, KEY_C1_Pin)==0){
			symbol='q';		LCD_Instruction (0b00000001, 0);
			LCD_Instruction (symbol, 1);
			HAL_Delay(1000);
			if(HAL_GPIO_ReadPin(KEY_C1_GPIO_Port, KEY_C1_Pin)==0){
				symbol='r';		LCD_Instruction (0b00000001, 0);
				LCD_Instruction (symbol, 1);
				HAL_Delay(1000);
				if(HAL_GPIO_ReadPin(KEY_C1_GPIO_Port, KEY_C1_Pin)==0){
					symbol='s';		LCD_Instruction (0b00000001, 0);
					LCD_Instruction (symbol, 1);
					HAL_Delay(1000);
				}
			}

		}
		flag=0;
	}

if(HAL_GPIO_ReadPin(KEY_C2_GPIO_Port, KEY_C2_Pin)==0)
	{
		symbol='t';		LCD_Instruction (0b00000001, 0);
		LCD_Instruction (symbol, 1);
		HAL_Delay(1000);
		if(HAL_GPIO_ReadPin(KEY_C2_GPIO_Port, KEY_C2_Pin)==0){
			symbol='u';		LCD_Instruction (0b00000001, 0);
			LCD_Instruction (symbol, 1);
			HAL_Delay(1000);
			if(HAL_GPIO_ReadPin(KEY_C2_GPIO_Port, KEY_C2_Pin)==0){
				symbol='v';		LCD_Instruction (0b00000001, 0);
				LCD_Instruction (symbol, 1);
				HAL_Delay(1000);
			}

		}
		flag=0;
	}

if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0)
	{
		symbol='w';		LCD_Instruction (0b00000001, 0);
		LCD_Instruction (symbol, 1);
		HAL_Delay(1000);
		if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0){
			symbol='x';		LCD_Instruction (0b00000001, 0);
			LCD_Instruction (symbol, 1);
			HAL_Delay(1000);
			if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0){
				symbol='y';		LCD_Instruction (0b00000001, 0);
				LCD_Instruction (symbol, 1);
				HAL_Delay(1000);
				if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0){
					symbol='z';		LCD_Instruction (0b00000001, 0);
					LCD_Instruction (symbol, 1);
					HAL_Delay(1000);
				}
			}

		}
		flag=0;
	}

HAL_GPIO_WritePin (KEY_R1_GPIO_Port,KEY_R1_Pin, SET);
HAL_GPIO_WritePin (KEY_R2_GPIO_Port,KEY_R2_Pin, SET);
HAL_GPIO_WritePin (KEY_R3_GPIO_Port,KEY_R3_Pin, SET);
HAL_GPIO_WritePin (KEY_R4_GPIO_Port,KEY_R4_Pin, RESET);

if(HAL_GPIO_ReadPin(KEY_C3_GPIO_Port, KEY_C3_Pin)==0)
	{
		symbol='#';		LCD_Instruction (0b00000001, 0);
		LCD_Instruction (symbol, 1);
		HAL_Delay(1000);
		flag=0;
	}
}//zamkniecie while
return symbol;

}//zamkniecie alphanum



/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  LCD_Init();
  HAL_Delay(50);
  LCD_Instruction (0b00111000, 0);
  HAL_Delay(50);
  LCD_Instruction (0b00001110, 0);
  HAL_Delay(50);
  LCD_Instruction (0b00000110, 0);
  HAL_Delay(50);
  LCD_Instruction (0b00000001, 0);

  word("it works!");


    char znak;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  { znak=alphanum();


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LCD_E_Pin|LCD_RS_Pin|LCD_D3_Pin|KEY_R4_Pin
                          |LCD_D0_Pin|LCD_D1_Pin|LCD_D4_Pin|LCD_D5_Pin
                          |LCD_D7_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LD2_Pin|KEY_R1_Pin|KEY_R2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LCD_D6_GPIO_Port, LCD_D6_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, KEY_R3_Pin|LCD_D2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LCD_E_Pin LCD_RS_Pin LCD_D3_Pin KEY_R4_Pin
                           LCD_D0_Pin LCD_D1_Pin LCD_D4_Pin LCD_D5_Pin
                           LCD_D7_Pin */
  GPIO_InitStruct.Pin = LCD_E_Pin|LCD_RS_Pin|LCD_D3_Pin|KEY_R4_Pin
                          |LCD_D0_Pin|LCD_D1_Pin|LCD_D4_Pin|LCD_D5_Pin
                          |LCD_D7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : LD2_Pin KEY_R1_Pin KEY_R2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin|KEY_R1_Pin|KEY_R2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : KEY_C3_Pin */
  GPIO_InitStruct.Pin = KEY_C3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(KEY_C3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : KEY_C2_Pin KEY_C1_Pin */
  GPIO_InitStruct.Pin = KEY_C2_Pin|KEY_C1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : LCD_D6_Pin */
  GPIO_InitStruct.Pin = LCD_D6_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LCD_D6_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : KEY_R3_Pin LCD_D2_Pin */
  GPIO_InitStruct.Pin = KEY_R3_Pin|LCD_D2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
