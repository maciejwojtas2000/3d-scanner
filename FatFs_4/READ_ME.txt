************************************
	Autor: Adam Jankowiak
	Przedmiot: SR
	Data: 12.04.2022	
************************************
Instrukcja co należy zrobić
PAMIĘTAJ żeby skopiować odpowiednie pliki z jednego projektu, nie mieszaj ich!

źródła:

Dokładne wyjaśnienie jak działa cała biblioteka + opis wszystkich funkcji.
WARTO PRZECZYTAĆ! 
http://elm-chan.org/fsw/ff/00index_e.html  

https://github.com/eziya/STM32_SPI_SDCARD/blob/master/STM32F4_HAL_SPI_SDCARD/Src/user_diskio.c
https://blog.naver.com/eziya76/221188701172
https://blog.domski.pl/using-fatfs-with-hal/



1.Inicjalizacja SPI
- Mode: Full-Duplex Master
- Data Size: 8bit
- prescaler: 8 (9.0MBits/s)

2. Ustawienie zegara na 72Mhz (Może działa z 80MHz)

3. FATFS - User-defined
- USE_LNF: Enabled with static working buffer on the BSS
- SPI2 global interrupt: Enabled

4. Ustaw pin SD_CD tam gdzie chcesz jako GPIO output
GPIO output level: High
Maximum output speed: Medium

5.Dodawanie plików
Core
    -> Inc
	  -> fatfs_sd.h
		- Dodaj plik (nie istnieje)
    -> Src
	  -> fatfs_sd.c
		- Dodaj plik (nie istnieje)

	  -> main.c
		- Najlepiej przkopiuj całość 

	  -> stm32l4xx_it.c
		- dodaj 2 Timery jak w przykładzie
FATFS
    -> Target
	     -> user_diskio.c 
			- Zmień return-y 
			- Dodaj #include "fatfs_sd.h"







Co zrobić, jeżeli nie działa?

1. Sprawdz poprawność połączeń.
2. Przed włożeniem karty mikrokontroler musi być odłączony od zasilania.
3. Wszystkie pliki muszą pochodzić z jednego projektu. 
4. Lee Ji-hoon lubi zmieniać piny na wyjściu CS, zwróć na to uwagę!
5. Wykorzystaj karty SD 32Gb.
