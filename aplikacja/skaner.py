from re import X
from tkinter import Y
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import csv
import os

#plik=open("filizanka.csv")
#dane=plik.read()
#dane=dane.split('\n')
#dane=[l.split(',') for l in dane]
#plik.close()
#header=dane.pop(0)
#dane.pop(0)


#x=tuple(map(int,dane[0]))
#y=tuple(map(int,dane[1]))
#z=tuple(map(int,dane[2]))

nazwa = input("Wprowadz nazwe pliku: ")


lines = []



with open(nazwa,"r") as f:
    lines = f.readlines()
    if f.readable():
        print("Prawidlowo podano nazwe pliku\r\n")
    else:
        print("Nie")

f.close()

tab = []

for i in lines:
    i = i.replace("\n","")
    tab.append(i.split(","))

x = []
y = []
z = []

for i in range(len(tab)):
    x.append(int(tab[i][0]))
    y.append(int(tab[i][1]))
    z.append(int(tab[i][2]))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

#x =[1,1,5,2,4,5,6,4,2,2]
#y =[5,6,2,3,1,4,1,2,4,8]
#z =[2,3,3,3,5,7,9,1,9,1]

ax.scatter(x, y, z, c='r', marker='o')

ax.set_xlabel('os x')
ax.set_ylabel('os y')
ax.set_zlabel('os z')

plt.show()
